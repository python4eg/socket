import datetime
import socket

IP = '127.0.0.1'
PORT = 23456

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((IP, PORT))
    s.listen()
    while True:
        connection, client = s.accept()
        print(f'[{datetime.datetime.now().strftime("%d %b %y %H:%M:%S")}] Connected with: {client!r}')
        with connection:
            all_data = b''
            while True:
                data = connection.recv(128)
                all_data += data
                if not data or data == b'ALL!':
                    break
            print(all_data)
            connection.sendall(b'Got it. Thanks.')
