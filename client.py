import datetime
import random
import socket
from time import sleep

IP = '127.0.0.1'
PORT = 23456

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((IP, PORT))
    sleep(1)
    nick = f'Nickname {int(random.random()*10)}'
    print(f'[{datetime.datetime.now().strftime("%d %b %y %H:%M:%S")}] Connected')
    s.send(bytes(nick, encoding='utf-8'))
    sleep(5)
    s.send(b'MESSAGE!')
    sleep(5)
    while True:
        print(s.recv(1024))
        data = input(f'{nick}: ')
        s.send(bytes(data, encoding='utf-8'))
